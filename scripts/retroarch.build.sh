#!/bin/sh

rm /build/AppDir -r

cd /build/src/RetroArch && \
    make clean && \
    /build/src/RetroArch/configure \
        --prefix=/usr \
        --disable-cdrom \
        --enable-ffmpeg \
        --enable-ssa \
        --enable-qt \
        --enable-lua \
        --enable-vulkan \
        --enable-hid && \
    make -j8 install DESTDIR=/build/AppDir &&
    cd /build &&
    /build/linuxdeploy-x86_64.AppImage --appdir /build/AppDir --output appimage

