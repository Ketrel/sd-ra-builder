#!/bin/sh
/build/src/RetroArch/configure \
    --prefix=/usr \
    --disable-cdrom \
    --enable-ffmpeg \
    --enable-ssa \
    --enable-qt \
    --enable-lua \
    --enable-vulkan \
    --enable-hid 

